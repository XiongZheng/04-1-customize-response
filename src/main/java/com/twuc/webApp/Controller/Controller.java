package com.twuc.webApp.Controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    //2.1
    @GetMapping("/api/no-return-value")
    public void getVoidStatus(){}

    //2.2
    @GetMapping("/api/no-return-value-with-annotation")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void getDefineStatus(){}

    //2.3
    @GetMapping("/api/messages/{message}")
    public String returnMessage(@PathVariable String message){
        return message;
    }

    //2.4
    @GetMapping("/api/message-objects/{message}")
    public Message returnMessageObject(@PathVariable String message){
        return new Message(message);
    }

    //2.5
    @GetMapping("/api/message-objects-with-annotation/{message}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Message returnStatus202(@PathVariable String message){
        return new Message(message);
    }

    //2.6
    @GetMapping("/api/message-entities/{message}")
    public ResponseEntity returnResponseEntity(@PathVariable String message){
        return ResponseEntity.created(null)
                .header("X-Auth","me")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(new Message(message));
    }
}
