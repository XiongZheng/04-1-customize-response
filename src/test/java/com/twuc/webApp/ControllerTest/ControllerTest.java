package com.twuc.webApp.ControllerTest;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class ControllerTest {

    @Autowired
    public MockMvc mockMvc;

    //2.1
    @Test
    void should_ensure_void_Response_status() throws Exception {
        mockMvc.perform(get("/api/no-return-value"))
                .andExpect(status().is(200));
    }

    //2.2
    @Test
    void should_define_status() throws Exception {
        mockMvc.perform(get("/api/no-return-value-with-annotation"))
                .andExpect(status().is(204));
    }

    //2.3
    @Test
    void should_return_string() throws Exception {
        mockMvc.perform(get("/api/messages/test"))
        .andExpect(status().isOk())
        .andExpect(content().string("test"));
    }

    //2.4
    @Test
    void should_return_object() throws Exception {
        mockMvc.perform(get("/api/message-objects/test"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string("{\"value\":\"test\"}"));
    }

    //2.5
    @Test
    void should_return_object_with_annotation() throws Exception {
        mockMvc.perform(get("/api/message-objects-with-annotation/test"))
                .andExpect(status().isAccepted())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string("{\"value\":\"test\"}"));
    }

    //2.6
    @Test
    void should_return_response_entity() throws Exception {
        mockMvc.perform(get("/api/message-entities/test"))
                .andExpect(status().isCreated())
                .andExpect(header().string("X-Auth","me"))
                .andExpect(content().string("{\"value\":\"test\"}"));
    }
}
